package main

import (
	"bytes"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

type Scenario struct {
	Args   []string
	Output string
	Title  string
}

func TestErrorScenarios(t *testing.T) {
	setup := func(scenario *Scenario) {
		os.Args = scenario.Args
		outErr = bytes.NewBuffer(nil)
		outOk = bytes.NewBuffer(nil)
	}

	for _, scenario := range []Scenario{
		{
			Title:  "Missing cron expression",
			Args:   []string{""},
			Output: "Missing cron expression",
		},
		{
			Title:  "Missing hour expression",
			Args:   []string{"*"},
			Output: "Insufficient subexpressions, got 1 but 6 expected",
		},
		{
			Title:  "Missing day of month expression",
			Args:   []string{"* *"},
			Output: "Insufficient subexpressions, got 2 but 6 expected",
		},
		{
			Title:  "Missing month expression",
			Args:   []string{"* * *"},
			Output: "Insufficient subexpressions, got 3 but 6 expected",
		},
		{
			Title:  "Missing day of week expression",
			Args:   []string{"* * * *"},
			Output: "Insufficient subexpressions, got 4 but 6 expected",
		},
		{
			Title:  "Missing command expression",
			Args:   []string{"* * * * *"},
			Output: "Insufficient subexpressions, got 5 but 6 expected",
		},
	} {
		t.Run(scenario.Title, func(t *testing.T) {
			setup(&scenario)
			main()

			outputOk := outOk.(*bytes.Buffer).String()
			assert.Equal(t, outputOk, "", "expected empty stdout, got %s", outputOk)

			outputErr := strings.TrimSpace(outErr.(*bytes.Buffer).String())
			assert.Equal(t, outputErr, scenario.Output, "unexpected error message")
		})
	}
}
