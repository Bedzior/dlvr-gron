package main

import (
	"fmt"
	"io"
	"os"

	"gron/pkg/cron_expression"
)

var outOk io.Writer = os.Stdout
var outErr io.Writer = os.Stderr

func main() {
	expression := os.Args[0]

	if expression == "" {
		fmt.Fprintln(outErr, "Missing cron expression")
	} else {
		cron_expression, err := cron_expression.Parse(expression)
		if err != nil {
			fmt.Fprintln(outErr, err)
			return
		}
		expanded, err := cron_expression.Expand()
		if err != nil {
			fmt.Fprintln(outErr, err)
			return
		}
		fmt.Fprint(outOk, expanded)
	}
}
