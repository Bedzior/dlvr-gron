package cron_field

// Cron expression bounds representation, both inclusive
type bounds struct {
	Lower uint32 // inclusive
	Upper uint32 // inclusive
}

type CronField struct {
	exp    string
	header string
	bounds bounds
}

type Expandible interface {
	Expand() (string, error)
}

func (c *CronField) Header() string {
	return c.header
}

func (c *CronField) Expand() (string, error) {
	// TODO actually perform the expansion
	return c.exp, nil
}

func NewMinuteField(exp string) CronField {
	return CronField{
		exp:    exp,
		header: "minute",
		bounds: bounds{0, 59},
	}
}
func NewHourField(exp string) CronField {
	return CronField{
		exp:    exp,
		header: "hour",
		bounds: bounds{0, 23},
	}
}
func NewDayOfWeekField(exp string) CronField {
	return CronField{
		exp:    exp,
		header: "day of week",
		bounds: bounds{0, 7},
	}
}
func NewMonthField(exp string) CronField {
	return CronField{
		exp:    exp,
		header: "month",
		bounds: bounds{1, 12},
	}
}
func NewDayOfMonthField(exp string) CronField {
	return CronField{
		exp:    exp,
		header: "day of month",
		bounds: bounds{1, 31},
	}
}
func NewCommandField(exp string) CronField {
	return CronField{
		exp:    exp,
		header: "command",
	}
}
