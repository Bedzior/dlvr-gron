package cron_expression

import (
	"fmt"
	"strings"

	"gron/pkg/cron_field"
)

// Stores a semi-parsed cron expression representation
type CronExpression struct {
	cronField [6]cron_field.CronField
}

func Parse(expression string) (*CronExpression, error) {
	// TODO refactor with channels, for the sake of it
	expressions := strings.Split(expression, " ")
	if len(expressions) != 6 {
		return nil, fmt.Errorf("Insufficient subexpressions, got %d but %d expected", len(expressions), 6)
	}
	cron_expression := &CronExpression{}
	cron_expression.cronField[0] = cron_field.NewMinuteField(expressions[0])
	cron_expression.cronField[1] = cron_field.NewHourField(expressions[1])
	cron_expression.cronField[2] = cron_field.NewDayOfMonthField(expressions[4])
	cron_expression.cronField[3] = cron_field.NewMonthField(expressions[3])
	cron_expression.cronField[4] = cron_field.NewDayOfWeekField(expressions[2])
	cron_expression.cronField[5] = cron_field.NewCommandField(expressions[5])

	return cron_expression, nil
}

func (c *CronExpression) Expand() (result string, err error) {
	longest := 0
	for _, field := range c.cronField {
		headerLength := len(field.Header())
		if headerLength > longest {
			longest = headerLength
		}
	}
	for _, field := range c.cronField {
		expanded, err := field.Expand()
		if err != nil {
			return "", fmt.Errorf("%w", err) // TODO prepend erroneous subexpression
		}
		result += fmt.Sprintf("%-*s   %s\n", longest, field.Header(), expanded)
	}
	return
}
