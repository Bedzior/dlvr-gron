```

         &@@@@@@@@@@@@@@@@@@      @@@@@@@@@@@@@@@@       /@@@@@@@@@@@@@@,      @@@@@@@          @@@@
      @@@@@@&     .@@@@@@@&       @@@@@/     .@@@@@   #@@@@@         @@@@@@@   &@@@@@@@@@.      @@@@
      @@@@@@.           ,@@@@@@   @@@@@, @@@@@@@#     @@@@@/           @@@@@%  (@@@@# *@@@@@@@( @@@@
        @@@@@@#         .@@@@@@   @@@@@.   @@@@@        @@@@@@@#    %@@@@@@    #@@@@%      @@@@@@@@@
            @@@@@@@@@@@@@@@       @@@@@      @@@@@.         (@@@@@@@@@#        @@@@@,           @@@@
                                                @@@@@

    thanks to https://manytools.org/hacker-tools/convert-images-to-ascii-art/
```


# Cron Expression Parser #
> [...] a command line application or script which parses a cron string and expands each field to show the times at which it will run.

but written and tested in Golang.

## Setup ##
```bash
sudo apt-get install go -y
# etc
```

## Usage ##
Pass a quoted cron string as the sole parameter to the script:
```bash
go run cmd/gron/gron.go ＂*/15 0 1,15 * 1-5 /usr/bin/find＂
```

which should in turn yield the following output:
```
minute         0 15 30 45
hour           0
day of month   1 15
month          1 2 3 4 5 6 7 8 9 10 11 12
day of week    1 2 3 4 5
command        /usr/bin/find
```

## Testing ##
```bash
go test
```

## Contribution guide ##
Maintain code readability.
Keep the tests up to date (CI is always watching).
Keep the codebase clean with auto format.

## Improvement ideas ##
&hellip;
