.DEFAULT_GOAL   := build
BINARY          := "cmd/gron/gron.go"

.PHONY: lint test run
.PRECIOUS: build

lint:
	go vet ${BINARY}

build: lint
	go build -o build/gron ${BINARY}

test: build
	go test ./... -v
